# install packages if not allready installed

neededPackage <- c("palmerpenguins", "starwarsdb", "kableExtra")

newPackages <- neededPackage[!(neededPackage %in% installed.packages()[,"Package"])]
if(length(newPackages))
  install.packages(newPackages, dependencies = T)

#devtools::install_github('davidgohel/ggiraph')